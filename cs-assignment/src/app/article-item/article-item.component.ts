import { Component, OnInit, Input } from '@angular/core';
import { Article } from '../shared/article.model';
import { ArticleHttpService } from '../shared/articlehttp.service';
import { ArticleService } from '../shared/articles.service';
import { CartBody } from '../shared/cartbody.model';

@Component({
  selector: 'app-article-item',
  templateUrl: './article-item.component.html',
  styleUrls: ['./article-item.component.css']
})
export class ArticleItemComponent implements OnInit {

  cartItem: CartBody;
  token: string;

  @Input()
  article: Article;

  constructor(private articleHttp: ArticleHttpService, private articleService: ArticleService) {}

  getLocalStorage = (key: string) => localStorage.getItem(key);

  ngOnInit() {
    this.token = this.getLocalStorage('token');
  }

  onBuyClicked(articleId: number) {
    const clickedArticle = this.articleService.getArticle(articleId);
    this.cartItem = new CartBody([{article_id: clickedArticle.id, article_quantity: clickedArticle.quantity}], 'de', 'EUR', 'de');

    this.articleHttp.addItemsToCart(this.token, this.cartItem).subscribe(
      (response) => {
        alert(clickedArticle.name + ' was added to shopping-cart.');
        console.log(response);
      },
      (error) => console.log(error)
    );
  }

}
