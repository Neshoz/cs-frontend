import { Component, OnInit } from '@angular/core';
import { ArticleHttpService } from './shared/articlehttp.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  constructor(private articleHttp: ArticleHttpService) {}

  setLocalStorage = (key: string, value: string) => localStorage.setItem(key, value);

  ngOnInit() {
    return this.articleHttp.getToken().subscribe(
      (response) => this.setLocalStorage('token', response.token),
      (error) => console.log(error)
    );
  }

}
