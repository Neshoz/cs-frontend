import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { ArticleHttpService } from './shared/articlehttp.service';
import { ArticlesComponent } from './articles/articles.component';
import { ArticleItemComponent } from './article-item/article-item.component';
import { ArticleService } from './shared/articles.service';

@NgModule({
  declarations: [
    AppComponent,
    ArticlesComponent,
    ArticleItemComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [ArticleHttpService, ArticleService],
  bootstrap: [AppComponent]
})
export class AppModule { }
