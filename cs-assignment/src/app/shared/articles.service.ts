import { Article } from './article.model';

export class ArticleService {

    articles: Article[] = [
        new Article(88, 1, 'Tongue-Smiley', 200, '../../assets/tongue-smiley.png'),
        new Article(102, 1, 'Happy-Smiley', 179, '../../assets/happy-smiley.png')
    ];

    getArticles() {
        return this.articles;
    }

    getArticle(id: number) {
        const article: Article = this.articles.find(
            (a) => {
                return a.id === id;
            }
        );
        return article;
    }
}
