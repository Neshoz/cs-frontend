import { Article } from './article.model';
export class CartBody {
    constructor(
        public articles: [{article_id: number, article_quantity: number}],
        public language_code: string,
        public currency_code: string,
        public country_code: string
    ) {}
}
