export class Article {
    constructor(public id: number, public quantity: number, public name: string, public price: number, public image: string) {}
}
