import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/Rx';
import { CartBody } from './cartbody.model';
const TOKEN_URL = 'https://xcaxla5hr1.execute-api.eu-west-1.amazonaws.com/v1/';
const CART_BASE_URL = 'https://xcaxla5hr1.execute-api.eu-west-1.amazonaws.com/v1/carts/';

@Injectable()
export class ArticleHttpService {

    constructor(private http: Http) {}

    getToken() {
        return this.http.post(TOKEN_URL, {'country_code': 'de'})
            .map((response: Response) => {
                return response.json();
            });
    }

    addItemsToCart(token: string, cartbody: CartBody) {
        return this.http.put(CART_BASE_URL + token, cartbody)
            .map((response: Response) => {
                return response.json();
            });
    }
}
