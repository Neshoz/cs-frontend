import { CsAssignmentPage } from './app.po';

describe('cs-assignment App', () => {
  let page: CsAssignmentPage;

  beforeEach(() => {
    page = new CsAssignmentPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
